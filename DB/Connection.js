const mongoose = require('mongoose');

const URI ="mongodb+srv://MyMongoDBUser:mongodb123@cluster0.xjczn.mongodb.net/Cluster0?retryWrites=true&w=majority";

const connectDB = async () => {
  await mongoose.connect(URI, {
    useUnifiedTopology: true,
    useNewUrlParser: true
  });
  console.log('db connected..!');
};

module.exports = connectDB;
